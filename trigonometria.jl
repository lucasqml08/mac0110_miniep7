# MAC0110 - MiniEP7
# Lucas Quaresma Medina Lam - 11796399

function seno(x)

    accuracy = 0.000001
    sinx = x
    valsin = sin(x)
    i = 1
    actual = x

    while accuracy <= abs(valsin - sinx)

        actual = -actual * x * x / (2 * i * (2 * i + 1))

        sinx = sinx + actual

        i = i + 1
    end

    return sinx

end

function cosseno(x)

    accuracy = 0.000001
    actual = 1
    cosx = 1
    valcos = cos(x)
    i = 1

    while accuracy <= abs(valcos - cosx)

        actual = -actual * x * x / (2 * i * (2 * i - 1))

        cosx = cosx + actual

        i = i + 1
    end

    return cosx

end


function bernoulli(n)
	n *= 2
	A = Vector{Rational{BigInt}}(undef, n+1)

	for f = 0 : n
		A[f+1] = 1 // (f + 1)
		for j = f : -1 : 1
			A[j] = j * (A[j] - A[j+1])
		end
	end

	return abs(A[1])

end

function fatorial(k)
    
    f = 1
    i = 1

    while i <= k
        f = f * i
        i = i + 1
	end

	return(f)

end

function tangente(x)
    
    actual = 0 
    tanx = 0
    n = 1

    while n <= 6
        
        ni = BigInt((2^(2 * n)) * ((2^(2 * n)) - 1))
        bn = bernoulli(n)
        xn = BigFloat((x^(2 * n - 1)))

        actual = (ni * bn * xn) / (fatorial(2*n))

        tanx = tanx + actual

        n = n + 1
    end

    return BigFloat(tanx)

end

function quaseigual(a, b)
    
    error = 0.1

    if abs(a - b) <= error
        return true

    else
        return false
    
    end

end

function check_sin(value, x)

    x = seno(x)

    if quaseigual(value, x)
        return true

    else 
        return false
        
    end

end

function check_cos(value, x)

    x = cosseno(x)
    
    if quaseigual(value, x)
        return true

    else 
        return false
        
    end

end

function check_tan(value, x)

    x = tangente(x)
    
    if quaseigual(value, x)
        return true

    else 
        return false
        
    end

end

function taylor_sin(x)

    accuracy = 0.000001
    sinx = x
    valsin = sin(x)
    i = 1
    actual = x

    while accuracy <= abs(valsin - sinx)

        actual = -actual * x * x / (2 * i * (2 * i + 1))

        sinx = sinx + actual

        i = i + 1
    end

    return sinx

end

function taylor_cos(x)

    accuracy = 0.000001
    actual = 1
    cosx = 1
    valcos = cos(x)
    i = 1

    while accuracy <= abs(valcos - cosx)

        actual = -actual * x * x / (2 * i * (2 * i - 1))

        cosx = cosx + actual

        i = i + 1
    end

    return cosx

end

function taylor_tan(x)
    
    actual = 0 
    tanx = 0
    n = 1

    while n <= 6
        
        ni = BigInt((2^(2 * n)) * ((2^(2 * n)) - 1))
        bn = bernoulli(n)
        xn = BigFloat((x^(2 * n - 1)))

        actual = (ni * bn * xn) / (fatorial(2*n))

        tanx = tanx + actual

        n = n + 1
    end

    return BigFloat(tanx)

end

using Test
function test()

    @test (sin(pi/6) - seno(pi/6)) <= 0.001
    @test (sin(pi/4) - seno(pi/4)) <= 0.001
    @test (sin(pi/2) - seno(pi/2)) <= 0.001
    @test (cos(pi/6) - cosseno(pi/6)) <= 0.001
    @test (cos(pi/4) - cosseno(pi/4)) <= 0.001
    @test (cos(pi/2) - cosseno(pi/2)) <= 0.001
    @test (tan(pi/6) - tangente(pi/6)) <= 0.1
    @test (tan(pi/4) - tangente(pi/4)) <= 0.1
    @test (tan(pi/3) - tangente(pi/3)) <= 0.1

    @test check_sin(0.5, pi/6) 
    @test check_sin(0.7, pi/4) 
    @test check_sin(1.0, pi/2) 
    @test check_cos(0.5, pi/3) 
    @test check_cos(0.7, pi/4) 
    @test check_cos(1.0, 2*pi)
    @test check_tan(0.6, pi/6)
    @test check_tan(1.0, pi/4)
    @test check_tan(1.7, pi/3)

end